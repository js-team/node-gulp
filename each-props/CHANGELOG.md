# Changelog

## [2.0.0](https://www.github.com/gulpjs/each-props/compare/v1.3.2...v2.0.0) (2021-09-27)


### ⚠ BREAKING CHANGES

* Normalize repository, dropping node <10.13 support (#5)

### Miscellaneous Chores

* Normalize repository, dropping node <10.13 support ([#5](https://www.github.com/gulpjs/each-props/issues/5)) ([779fed2](https://www.github.com/gulpjs/each-props/commit/779fed265bb7c0b67d1030ad8c2b9b4136883601))
